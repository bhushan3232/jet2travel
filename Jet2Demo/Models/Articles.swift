//
//  Articles.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation
// Represents a single Article model
struct Article: Codable {
    var id: String?
    var createdAt: String?
    var content: String?
    var comments: Int?
    var likes: Int?
    var media: [ArticleMedia]?
    var user: [User]?
}
// Represents model for Articles media
struct ArticleMedia: Codable {
    var id: String?
    var blogId: String?
    var createdAt: String?
    var image: String?
    var title: String?
    var url: String?
}
// Represents model for an article user
struct User: Codable {
    var id: String?
    var blogId: String?
    var createdAt: String?
    var name: String?
    var avatar: String?
    var lastname: String?
    var city: String?
    var designation: String?
    var about: String?
}
