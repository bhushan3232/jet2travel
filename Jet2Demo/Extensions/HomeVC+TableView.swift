//
//  HomeVC+TableView.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articlesViewModel.articles?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let articleCell: ArticleCell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.cellIdentifier.rawValue) as? ArticleCell, let article = articlesViewModel.articles?[indexPath.row] {
            return articlesViewModel.setupUI(for: articleCell, with: article)
        }
        return ArticleCell()
    }
    func changeIndicatorStatus() {
        DispatchQueue.main.async {
            if self.articlesViewModel.isLoading {
                self.indicator.startAnimating()
                self.view.isUserInteractionEnabled = false
            } else {
                self.indicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
}
