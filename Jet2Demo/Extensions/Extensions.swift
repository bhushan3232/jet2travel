//
//  Extensions.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation
// MARK: Date Extensions

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
}
// MARK: String Extensions

extension String {
    // Returns date string in Local time
    func UTCToLocalTime(receivedFormat: String, convertedFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = receivedFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let date = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = convertedFormat
        return dateFormatter.string(from: date ?? Date())
    }
    /// Returns date from String
    func date(from dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormats.CONVERTED.rawValue
        return dateFormatter.date(from: dateString) ?? Date()
    }
}
