//
//  ArticlesViewModel.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import UIKit
import SDWebImage

class ArticlesViewModel {
    private let networkManager = NetworkManager.shared
    var articles: [Article]?
    var isLoading = true
    var currentPage: String = "1"
    // MARK: Request Articles
    func requestBlogArticles(
        pageNumber: String,
        withLimit: String = Constants.Pagination.DEFAULTLIMIT.rawValue,
        completion: @escaping(
        _ response: [Article]?,
        _ success: Bool,
        _ errMessage: String?
        ) -> ()
    ) {
        networkManager.getBlogArticles(pageNumber: pageNumber) { (articlesData, status, message) in
            if let results = articlesData, status == true {
                self.isLoading = false
                completion(results, status, message)
            } else {
                self.isLoading = false
                completion(nil, status, message)
            }
        }
    }
    // MARK: Set up Cell UI
    func setupUI(for cell: ArticleCell, with viewModel: Article) -> ArticleCell {
        // Get media image for article if url present
        if let mediaURL = viewModel.media?.first?.image {
            cell.mediaView.isHidden = false
            cell.mediaImageView.sd_setImage(with: URL(string: mediaURL), completed: nil)
        } else {
            // Do not show media image view
            cell.mediaView.isHidden = true
        }
        // Get user image for article if url present
        if let userAvtarURL = viewModel.user?.first?.avatar {
            cell.userImageView.sd_setImage(with: URL(string: userAvtarURL), completed: nil)
            updateImage(in: cell.userImageView, imageType: .AVTAR)
        }
        // Set all other values on UI
        cell.usernameLbl?.text = viewModel.user?.first?.name
        cell.articleContentTxtView.text = viewModel.content
        cell.likesLbl.text = "\(viewModel.likes ?? 0) \(Constants.LabelConstants.Likes.rawValue)"
        cell.commentsLbl.text = "\(viewModel.comments ?? 0) \(Constants.LabelConstants.Comments.rawValue)"
        if let articleUrl = viewModel.media?.first?.url {
            cell.articleUrlView.isHidden = false
            cell.articleUrlLbl.text = articleUrl
        } else { cell.articleUrlView.isHidden = true }
        if let articletitle = viewModel.media?.first?.title {
            cell.titleView.isHidden = false
            cell.articleTitleLbl.text = articletitle
        } else { cell.titleView.isHidden = true }

        cell.designationLbl.text =
            viewModel.user?.first?.designation ?? ""
        cell.createdLbl.text = processCreationDate(createdAt: viewModel.createdAt ?? "")
        return cell
    }
    // MARK: Set Avtar image mask
    func updateImage(in imageView: UIImageView,
                     imageType: Constants.ImageType = .MEDIA) {
        DispatchQueue.main.async {
            if imageType == .AVTAR {
                imageView.layer.cornerRadius = imageView.frame.height / 2
                imageView.clipsToBounds = true
            }
        }
    }
    // MARK: Create creation time label for article
    func processCreationDate(createdAt: String) -> String {
        if !createdAt.isEmpty {
            let convertedDate = createdAt.UTCToLocalTime(receivedFormat: Constants.DateFormats.RECEIVED.rawValue, convertedFormat: Constants.DateFormats.CONVERTED.rawValue)
            let creationDate = convertedDate.date(from: convertedDate)
            let years =  abs(creationDate.years(from: Date()))
            let months = abs(creationDate.months(from: Date()))
            let weeks =  abs(creationDate.weeks(from: Date()))
            let days =   abs(creationDate.days(from: Date()))
            let hrs =    abs(creationDate.hours(from: Date()))
            let mins =   abs(creationDate.minutes(from: Date()))
            if (years > 0) {
                return years > 1 ? "\(years) years" : "\(years) year"
            } else if (months > 0) {
                return months > 1 ? "\(months) months" : "\(months) month"
            } else if (weeks > 0) {
                return weeks > 1 ? "\(weeks) weeks" : "\(weeks) week"
            } else if (days > 0) {
                return days > 1 ? "\(days) days" : "\(days) day"
            } else if (hrs > 0) {
                return hrs > 1 ? "\(hrs) hrs" : "\(hrs) hr"
            } else if (mins > 0) {
                return mins > 1 ? "\(mins) mins" : "\(mins) min"
            } else {
                return "Just now"
            }
        }
        return ""
    }
    // MARK: Pagination Stuff
    func managePagination(pageNumber: String) {
        let currentPageNo = (Int(pageNumber) ?? 0) + 1
        currentPage = "\(currentPageNo)"
    }
}
