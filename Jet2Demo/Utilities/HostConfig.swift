//
//  HostConfig.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation

struct HostConfig {

    public static let hostBaseURL = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/"

    enum Endpoints: String {
        case blogArticles = "blogs?page=#&limit=%@"
    }
    enum RequestMethod: String {
        case GET = "GET"
        case POST = "POST"
    }
    enum RequestHeaderFields: String {
        case ContentType = "Content-Type"
        case Authorization = "Authorization"
    }
    enum HTTPHeaderValues: String {
        case ContentTypeValue = "text/json"
    }
}
enum Errors: String {
    case UnsupportedURL = "Unsupported URL"
    case Unknown = "Something went wrong"
}
