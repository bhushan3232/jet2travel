//
//  CommonUtility.swift
//  MVVM
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 cellpointmobile. All rights reserved.
//

import Foundation
import UIKit

class CommonUtility {
    static func displayErrorAlert(title: String = Constants.UtilityStrings.ERRTITLE.rawValue, message: String, viewController:UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Constants.UtilityStrings.BTNTITLEOK.rawValue, style: .cancel, handler: nil)
            alert.addAction(okAction)
            viewController.present(alert, animated: true, completion: nil)
        }
   }
}
