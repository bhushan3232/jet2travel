//
//  Constants.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation

struct Constants {
    enum Identifiers: String {
        case cellIdentifier = "articleCell"
    }
    enum NibNames: String {
        case ArticleCell = "ArticleCell"
    }
    enum URLType {
        case DATA
        case MEDIA
    }
    enum LabelConstants: String {
        case Likes = "Likes"
        case Comments = "Comments"
    }
    enum ImageType {
        case AVTAR
        case MEDIA
    }
    enum DateFormats: String {
        case RECEIVED = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        case CONVERTED = "MMM d, yyyy h:mm a"
    }
    enum Pagination: String {
        case DEFAULTLIMIT = "10"
    }
    enum UtilityStrings: String {
        case BTNTITLEOK = "OK"
        case ERRTITLE = "ERROR"
    }
}
