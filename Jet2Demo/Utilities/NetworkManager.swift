//
//  NetworkManager.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import Foundation
class NetworkManager: NSObject {
    // A singleton to perform network operations
    static let shared = NetworkManager()
    override private init() {
        super.init()
    }

    /// Fetches articles from the url
    /// - Parameters:
    ///   - pageNumber: Page number to fetch articles from
    ///   - withLimit: Limit indicates number of articles to be fetched, by default 10
    ///   - completion: A completion handler to pass back result to caller
    func getBlogArticles(pageNumber: String,
                         withLimit: String = Constants.Pagination.DEFAULTLIMIT.rawValue,
                         completion: @escaping(
        _ response: [Article]?,
        _ success: Bool,
        _ errMessage: String?
        ) -> ()) {
        let endpoint = (HostConfig.Endpoints.blogArticles.rawValue).replacingOccurrences(of: "#", with: pageNumber).replacingOccurrences(of: "%@", with: withLimit)
        makeAPIRequest(endPoint: endpoint,
                       withHTTPMethod: HostConfig.RequestMethod.GET.rawValue,
                       withBodyParams: nil) { (results, status, message) in
                        if let data = results, status {
                            do {
                                let articles = try JSONDecoder().decode([Article].self, from: data)
                                completion(articles, true, message)
                            } catch let parseError {
                                // handle error
                                print("Something went wrong!!", parseError.localizedDescription)
                                completion(nil, false, parseError.localizedDescription)
                            }
                        } else {
                            if let msg = message {
                                print("\(#function) \(msg)")
                                completion(nil, false, msg)
                            } else {
                                print("\(#function) \(Errors.Unknown.rawValue)")
                            }
                        }
        }
    }

    /// A reusable method to perform an API call and return the result
    /// - Parameters:
    ///   - endPoint: End point to be called
    ///   - withHTTPMethod: HTTP Method for given request
    ///   - withBodyParams: HTTP body if any otherwise nil
    ///   - authorization: Authorisation if any nil otherwise
    ///   - urlType: Type can be DATA or MEDIA
    ///   - completion: Completion handler to pass back result to caller
    func makeAPIRequest(endPoint: String,
                        withHTTPMethod: String,
                        withBodyParams: Data?,
                        authorization:String? = nil,
                        urlType: Constants.URLType = .DATA,
                        completion: @escaping(_ response: Data?, _ success: Bool, _ errMessage: String?) -> ()) {
        guard let url = urlType == .DATA ? URL(string: HostConfig.hostBaseURL.appending(endPoint)) : URL(string: endPoint) else {
            completion(nil,false,Errors.UnsupportedURL.rawValue)
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
        urlRequest.addValue(
            HostConfig.HTTPHeaderValues.ContentTypeValue.rawValue, forHTTPHeaderField: HostConfig.RequestHeaderFields.ContentType.rawValue
        )
        urlRequest.httpMethod = withHTTPMethod
        if let requestBody = withBodyParams  {
            urlRequest.httpBody = requestBody
        }

        if let authToken = authorization {
            urlRequest.addValue(authToken, forHTTPHeaderField:
                HostConfig.RequestHeaderFields.Authorization.rawValue)
        }
        let urlTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, let response = response as? HTTPURLResponse, error == nil
                else {
                    print("Something went wrong!!", error?.localizedDescription as Any)
                    completion(nil, false,error?.localizedDescription)
                    return
            }
            guard (200) ~= response.statusCode else {
                print("Something went wrong!!", response.statusCode)
                completion(nil, false,"\(response.statusCode)")
                return
            }
            completion(data, true,"Success")
        }
        urlTask.resume()
    }
}
