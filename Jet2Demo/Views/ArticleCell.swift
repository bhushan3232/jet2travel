//
//  ArticleCell.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {
    
    // UserInfoView Outlets
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var designationLbl: UILabel!
    @IBOutlet weak var createdLbl: UILabel!

    // MediaView Outlets
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaImageView: UIImageView!

    // ContentView Outlets
    @IBOutlet weak var articleContentView: UIView!
    @IBOutlet weak var articleContentTxtView: UITextView!

    // TitleView Outlets
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var articleTitleLbl: UILabel!

    // ArticleURLView Outlets
    @IBOutlet weak var articleUrlView: UIView!
    @IBOutlet weak var articleUrlLbl: UILabel!

    // LikesView Outlets
    @IBOutlet weak var likesView: UIView!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var commentsLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        usernameLbl.text = ""
        designationLbl.text = ""
        createdLbl.text = ""
        mediaImageView.image = nil
        userImageView.image = nil
        likesLbl.text = ""
        commentsLbl.text = ""
        articleUrlLbl.text = ""
        articleContentTxtView.text = ""
        articleTitleLbl.text = ""
    }

}
