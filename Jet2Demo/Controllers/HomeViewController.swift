//
//  ViewController.swift
//  Jet2Demo
//
//  Created by Bhushan Shinde on 15/08/20.
//  Copyright © 2020 geekyB. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // Outlets
    @IBOutlet weak var articlesTblView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    // Variables
    let articlesViewModel = ArticlesViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        requestBlogArticles()
    }
    // MARK: Set up Table View
    func setupTableView() {
        articlesTblView.register(UINib(nibName: Constants.NibNames.ArticleCell.rawValue, bundle: Bundle.main), forCellReuseIdentifier: Constants.Identifiers.cellIdentifier.rawValue)
    }
    // MARK: Request Articles 
    func requestBlogArticles() {
        articlesViewModel.requestBlogArticles(pageNumber: articlesViewModel.currentPage) { (blogArticles, status, message) in
            if let fetchedBlogArticles = blogArticles, status == true {
                if self.articlesViewModel.articles == nil {
                    self.articlesViewModel.articles = fetchedBlogArticles
                } else {
                    for blogArticle in fetchedBlogArticles {
                        self.articlesViewModel.articles?.append(blogArticle)
                    }
                }
                DispatchQueue.main.async {
                    self.articlesTblView.reloadData()
                    self.changeIndicatorStatus()
                }
            } else {
                // present error
                self.articlesViewModel.isLoading = false
                self.changeIndicatorStatus()
                CommonUtility.displayErrorAlert(message: message ?? Errors.Unknown.rawValue, viewController: self)
            }
        }
    }
    // MARK: Manage Pagination
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !articlesViewModel.isLoading){
            articlesViewModel.isLoading = true
            articlesViewModel.managePagination(pageNumber: articlesViewModel.currentPage)
            requestBlogArticles()
            changeIndicatorStatus()
        }
    }
}

